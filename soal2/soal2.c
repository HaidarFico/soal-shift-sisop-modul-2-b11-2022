#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Contains fork()
#include <sys/types.h>
#include <dirent.h>
#include <wait.h>
#include <string.h>
#include <sys/stat.h>

void unzip(char directoryBase[], char directoryDest[]){
    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *args[] = {"unzip","-q",directoryBase,"*.png","-d",directoryDest,NULL};
        execv("/usr/bin/unzip",args);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}

void rm(char directory[]){
    int status;
	pid_t child_id = fork();
    if (child_id < 0) {
    	exit(EXIT_FAILURE);
  	}
	if (child_id == 0) {
        char *args[] = {"rm","-r",directory,NULL};
        execv("/usr/bin/rm",args);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}

void createDir(char directory[]){
    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", directory, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}

int isFileExists(const char *path){
    // Try to open file
    FILE *fptr = fopen(path, "r");

    // If file does not exists 
    if (fptr == NULL)
        return 0;

    // File exists hence close file and return true.
    fclose(fptr);

    return 1;
}

void createTxt(char directory[], char judul[], char tahun[]){
    strcat(directory,"tmp.txt");

    // printf("%s\n", directory);

    FILE *fp = fopen(directory, "w");

    fprintf(fp, "%s %s\n", tahun, judul);

    fclose(fp);
}

void writeTxt(char directory[], char judul[], char tahun[]){
	strcat(directory,"tmp.txt");

	FILE *fp = fopen (directory, "a+");

	fprintf(fp, "%s %s\n", tahun, judul);

	fclose(fp);
}

void getGenre(char *name){
    // char dirPathShift[100]="/home/nethan/shift2/drakor/";
    // char dirPath[100]="/home/nethan/shift2/drakor/";
    char *userName = getlogin();
    char dirPathShift[100];
    char dirPath[100];
    strcpy(dirPathShift,"/home/");
    strcat(dirPathShift,userName);
    strcat(dirPathShift,"/shift2/drakor/");
    strcpy(dirPath,dirPathShift);

    int serialnumber = 0;
    char *contain;

    char *token = strtok(name, ";_");
    char judul[100];
    char tahun[100];
    while (token != NULL){
        if (serialnumber == 2){
            contain = strstr(token, ".png");
            if (contain){
                token[strlen(token)-4] = '\0';
            }
            // printf("1. %s\n", dirPath);
            strcat(dirPath,token);
            strcat(dirPath,"/");
            // printf("2. %s\n", dirPath);
            if (!isFileExists(dirPath)){
                createDir(dirPath);
                createTxt(dirPath,judul,tahun);
            } else {
                writeTxt(dirPath,judul,tahun);
            }
            strcpy(dirPath,dirPathShift);
            serialnumber = 0;
        }
        else if (serialnumber == 1){
            strcpy(tahun,token);
            serialnumber++;
        }
        else {
            strcpy(judul,token);
            serialnumber++;
        }
        token = strtok(NULL, ";_");
    }
}

void runThruFiles(const char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir) 
        return; 

    while ((dp = readdir(dir)) != NULL){ // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            getGenre(dp->d_name);
        }
    }

    // Close directory stream
    closedir(dir);
}

void copyPic(char *name, char *folder){
    char genre[BUFSIZ];

    char dirPathBase[100];
    char *tempName = (char*) malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathBase,"/home/");
    strcat(dirPathBase,tempName);

    char dirPathDest[100];
    strcpy(dirPathDest,dirPathBase);

    strcat(dirPathBase,"/tmp/");
    strcat(dirPathBase,name);
    strcat(dirPathDest,"/shift2/drakor/");
    strcat(dirPathDest,folder);
    
    int serialnumber = 0;
    char *contain;
    char judul[100];

    char *token = strtok(name, ";_");
    while (token != NULL){
        // contain = strstr(token, "_");
        if (serialnumber == 2){
            contain = strstr(token, ".png");
            if (contain){
                token[strlen(token)-4] = '\0';
            }
            // printf("1. %s\n", token);
            // printf("2. %s\n", folder);
            if (!strcmp(token,folder)){
                strcat(dirPathDest,"/");
                strcat(dirPathDest,judul);
                strcat(dirPathDest,".png");
                // printf("dest : %s\n", dirPathDest);
            }
            serialnumber = 0;
        }
        else if (serialnumber == 0){
            strcpy(judul,token);
            serialnumber++;
        }
        else{
            serialnumber++;
        }
        token = strtok(NULL, ";_");
    }

    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
        char *argv[] = {"cp", dirPathBase, dirPathDest, NULL};
        execv("/usr/bin/cp",argv);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}

void compareDir(const char *path, char *folderName){
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir) 
        return;

    char *contain;

    while ((dp = readdir(dir)) != NULL){ // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            contain = strstr(dp->d_name,folderName);
            if (contain){
                copyPic(dp->d_name,folderName);
            }
        }
    }
    // Close directory stream
    closedir(dir);
}

void sort(char directory[]){
    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"sort", "-o", directory, directory, NULL};
        execv("/usr/bin/sort", argv);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}

void scanTmp(char *folder){
    char dirPathTmp[100];
    char dirPathData[100];
    char *tempName = (char*) malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathTmp,"/home/");
    strcat(dirPathTmp,tempName);
    strcat(dirPathTmp,"/shift2/drakor/");
    strcat(dirPathTmp,folder);
    strcpy(dirPathData,dirPathTmp);
    strcat(dirPathTmp,"/tmp.txt");
    strcat(dirPathData,"/data.txt");

    sort(dirPathTmp);

    char line[256];
    char nama[100];
    char rilis[100];

    FILE *ftmp = fopen(dirPathTmp, "r");
    FILE *fdatawrite = fopen(dirPathData, "w");
    FILE *fdataappend = fopen(dirPathData, "a");

	fprintf(fdatawrite, "kategori : %s\n\n", folder);
    while (fscanf(ftmp, "%s %s", rilis, nama) != EOF){
        fprintf(fdataappend, "nama : %s\nrilis: tahun %s\n\n", nama, rilis);
    }
    
	fclose(ftmp);
    fclose(fdatawrite);
    fclose(fdataappend);

    rm(dirPathTmp);
}

void runThruFolders(const char *path, const char *tempName){
    struct dirent *dp;
    DIR *dir = opendir(path);

    // char dirPathTmp[100]="/home/nethan/tmp/";
    char dirPathTmp[100]="/home/";
    strcat(dirPathTmp, tempName);
    strcat(dirPathTmp,"/tmp/");

    // Unable to open directory stream
    if (!dir) 
        return;

    while ((dp = readdir(dir)) != NULL){ // Iterate every folder
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            compareDir(dirPathTmp,dp->d_name);
            scanTmp(dp->d_name);
        }
    }

    // Close directory stream
    closedir(dir);
}

int main(){
    char dirPathShift[100];
    char dirPathTmp[100];
    char *tempName = (char*) malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathShift,"/home/");
    strcat(dirPathShift,tempName);
    strcpy(dirPathTmp,dirPathShift);
    strcat(dirPathShift,"/shift2/drakor");
    strcat(dirPathTmp,"/tmp/");

    // a) unzip drakor.zip dan delete folder
    unzip("./drakor.zip",dirPathTmp);

    // b) buat folder untuk tiap genre
    runThruFiles(dirPathTmp);

    // c & d) copy semua file ke folder masing-masing
    runThruFolders(dirPathShift, tempName);
    rm(dirPathTmp);

    // e) tulis data.txt


    return 0;
}