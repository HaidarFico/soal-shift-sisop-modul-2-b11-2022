#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Contains fork()
#include <sys/types.h>
#include <dirent.h>
#include <wait.h>
#include <string.h>
#include <sys/stat.h>

int createDir(char directory[])
{
    // printf("%s\n",directory);
    char *argv[] = {"mkdir", "-p", directory, NULL};
    execv("/usr/bin/mkdir", argv);
    return 0;
}

int unzip(char directory[])
{
    char *arguments[] = {"unzip", "./animal.zip", "-d", directory, NULL};
    execvp("/usr/bin/unzip", arguments);
    return 0;
}

int sortAnimalEarth(char *dirList[], char directoryAnimal[], char directoryEarth[])
{
    int i;
    int counter = 1;
    char *arguments[120];
    for (int k = 0; k < 120; k++)
        arguments[k] = malloc(100);
    char tempString[40];
    arguments[0] = "mv";
    for (i = 1; i < 20; i++)
    {
        if ((strstr(dirList[i - 1], "_darat")))
        {
            // printf("This is the found dirList %s\n",dirList[i-1]);
            strcpy(tempString, directoryAnimal);
            strcat(tempString, dirList[i - 1]);
            // printf("this is tempstring %s\n", tempString);
            strcpy(arguments[counter], tempString);
            // printf("This is counter: %d\n",counter);
            // arguments[counter] = tempString;
            // arguments[counter] = dirList[i-1];
            // printf("This is argv in the loop %s\n",arguments[counter]);
            counter++;
        }
    }
    // printf("Nyampe sini\n");
    arguments[counter] = directoryEarth;
    arguments[counter + 1] = NULL;
    // for(int j = 0; j < 24; j++){
    // printf("This is argv: %s\n",arguments[j]);
    // printf("Loop Test\n");
    // }
    // for(int k = 0; k < 120; k++) free(arguments[k]);

    execv("/usr/bin/mv", arguments);
    return 0;
}

int sortAnimalWater(char *dirList[], char directoryAnimal[], char directoryWater[])
{
    int i;
    int counter = 1;
    char *arguments[120];
    for (int k = 0; k < 120; k++)
        arguments[k] = malloc(100);
    char tempString[40];
    arguments[0] = "mv";
    // for(int i = 0; i < 35; i++) printf("This is dirList %s\n",dirList[i]);
    for (i = 1; i < 12; i++)
    {
        if ((strstr(dirList[i - 1], "_air")))
        {
            strcpy(tempString, directoryAnimal);
            strcat(tempString, dirList[i - 1]);
            // printf("this is tempstring %s\n", tempString);
            strcpy(arguments[counter], tempString);
            // printf("This is counter: %d\n",counter);
            // arguments[counter] = tempString;
            // arguments[counter] = dirList[i-1];
            // printf("This is argv in the loop %s\n",arguments[counter]);
            counter++;
        }
    }

    arguments[counter] = directoryWater;
    arguments[counter + 1] = NULL;
    // for(int j = 0; j < 100; j++){
    //     printf("This is argv for directory water: %s\n",arguments[j]);
    // }
    // for(int k = 0; k < 120; k++) free(arguments[k]);

    execv("/usr/bin/mv", arguments);
    return 0;
}

int getDir(char *dirList[], char directory[])
{
    int i = 0;
    DIR *dp;
    struct dirent *ep;
    dp = opendir(directory);
    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            dirList[i] = ep->d_name;
            // printf("This is dirList %s\n",dirList[i]);
            i++;
        }
    }
    return 0;
}

int removeDir(char directory[])
{
    char *arguments[4];
    arguments[0] = "rm";
    arguments[1] = "-r";
    arguments[2] = directory;
    arguments[3] = NULL;
    execv("/usr/bin/rm", arguments);
}

int removeBird(char *dirList[], char directoryEarth[])
{
    // printf("\nwhoaaaaaa\n");
    int i;
    int counter = 2;
    char *arguments[120];
    for (int k = 0; k < 120; k++)
        arguments[k] = malloc(100);
    char tempString[40];
    arguments[0] = "rm";
    arguments[1] = "-r";
    for (i = 2; i < 14; i++)
    {
        // printf("This is dirList %s\n",dirList[i-2]);
        if ((strstr(dirList[i - 2], "_bird")))
        {
            strcpy(tempString, directoryEarth);
            strcat(tempString, dirList[i - 2]);
            // printf("this is tempstring %s\n", tempString);
            strcpy(arguments[counter], tempString);
            // printf("This is counter: %d\n",counter);
            // arguments[counter] = tempString;
            // arguments[counter] = dirList[i-1];
            // printf("This is argv in the loop %s\n",arguments[counter]);

            counter++;
        }
    }
    arguments[counter] = NULL;
    // for(int j = 0; j < 100; j++){
    //     printf("This is argv: %s\n",arguments[j]);
    // }

    // for(int k = 0; k < 120; k++) free(arguments[k]);

    execv("/usr/bin/rm", arguments);
    return 0;
}

void checkPermissions(char userPermissions[], struct stat *info)
{
    int counter = 0;
    if (info->st_mode & S_IRUSR)
    {
        userPermissions[counter] = 'r';
        counter++;
    }
    if (info->st_mode & S_IWUSR)
    {
        userPermissions[counter] = 'w';
        counter++;
    }
    if (info->st_mode & S_IXUSR)
    {
        userPermissions[counter] = 'x';
        counter++;
    }
    userPermissions[counter] = '\0';
    return;
}

int fileWrite(char *dirList[], char directoryWater[])
{
    int i = 0;
    char listFilePath[50];
    strcpy(listFilePath, directoryWater);
    strcat(listFilePath, "list.txt");
    FILE *fp = fopen(listFilePath, "w+");
    struct stat info;
    char filePath[100];
    char *username = getlogin();
    char userPermissions[4] = {'t', 'm', 'p', '\0'};
    while (i < 9)
    {
        // fprintf(fp,"Test\n");
        // printf("This is dirList: %s\n",dirList[i]);
        if (strstr(dirList[i], "..") || (strcmp(".", dirList[i]) == 0))
        {
            i++;
            continue;
        }
        strcpy(filePath, directoryWater);
        strcat(filePath, dirList[i]);
        // printf("This is filePath %s\n",filePath);
        stat(filePath, &info);
        checkPermissions(userPermissions, &info);
        fprintf(fp, "%s_%s_%s\n", username, userPermissions, dirList[i]);
        i++;
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    pid_t child_id[10];
    int status[10];
    char dirPathBase[100];
    char dirPathWater[100];
    char dirPathEarth[100];
    char dirPathAnimal[100];
    char *tempName = (char *)malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathBase, "/home/");
    strcat(dirPathBase, tempName);
    // free(tempName);
    strcat(dirPathBase, "/modul2");
    strcpy(dirPathEarth, dirPathBase);
    strcpy(dirPathWater, dirPathBase);
    strcpy(dirPathAnimal, dirPathBase);
    strcat(dirPathWater, "/air/");
    strcat(dirPathEarth, "/darat/");
    strcat(dirPathAnimal, "/animal/");
    // printf("Does this work lol: %s\n",dirPathWater);
    child_id[0] = fork();
    // printf("This is child id %d\n",child_id[0]);
    // Process membuat directory
    if (child_id[0] < 0)
        exit(EXIT_FAILURE);

    if (child_id[0] == 0)
    {
        child_id[1] = fork();
        // printf("This is the fork's child id %d\n",child_id[1]);
        if (child_id[1] < 0)
            exit(EXIT_FAILURE);

        if (child_id[1] == 0)
            if (createDir(dirPathEarth) == EXIT_FAILURE)
                exit(EXIT_FAILURE);
            else
            {
                // printf("Nyampe sini\n");
                exit(EXIT_SUCCESS);
            }
        else
        {
            while (wait(&status[1]) > 0)
                ;
            // printf("This is before sleep\n");
            sleep(3);
            // printf("This is after sleep\n");
            if (createDir(dirPathWater) == EXIT_FAILURE)
            {
                // printf("Nyampe sini gasi?");
                exit(EXIT_FAILURE);
            }
            else
            {
                exit(EXIT_SUCCESS);
            }
        }
    }

    while ((wait(&status[0])) > 0)
        ;

    // printf("aPA INI JALAN DULUAN\n");
    child_id[3] = fork();
    char *directoryList[256];
    char *tempDirectoryList[256];
    // printf("Testing123\n");

    if (child_id[3] < 0)
        exit(EXIT_FAILURE);

    if (child_id[3] == 0)
    {
        child_id[4] = fork();

        if (child_id[4] < 0)
            exit(EXIT_FAILURE);

        if (child_id[4] == 0)
        {

            if (unzip(dirPathBase) == EXIT_FAILURE)
            {
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
        // printf("Nyampe sini wey\n");
        if (child_id[4] > 0)
        {
            while (wait(&status[3]) > 0)
                ;
            // sleep(3);
            // printf("This is dirpathAnimal %s\n",dirPathAnimal);
            getDir(directoryList, dirPathAnimal);
            if (sortAnimalEarth(directoryList, dirPathAnimal, dirPathEarth) == EXIT_FAILURE)
            {
                exit(EXIT_FAILURE);
            }
        }

        // printf("Child process selesai\n");
        exit(EXIT_SUCCESS);
    }
    else
    {
        while (wait(&status[2]) > 0)
            ;
        sleep(3);

        child_id[5] = fork();

        if (child_id[5] < 0)
            exit(EXIT_FAILURE);

        if (child_id[5] == 0)
        {
            child_id[6] = fork();

            if (child_id[6] < 0)
                exit(EXIT_FAILURE);

            if (child_id[6] == 0)
            {

                getDir(directoryList, dirPathAnimal);

                // printf("Nyampe sini juga kok\n");
                if (sortAnimalWater(directoryList, dirPathAnimal, dirPathWater) == EXIT_FAILURE)
                {
                    exit(EXIT_FAILURE);
                }
            }

            else
            {
                while (wait(&status[5]) > 0)
                    ;
                // printf("Nyampe sini kok rek\n");
                if (removeDir(dirPathAnimal) == EXIT_FAILURE)
                {
                    exit(EXIT_FAILURE);
                }
            }
        }

        else
        {
            while (wait(&status[4]) > 0)
                ;
            child_id[8] = fork();
            if (child_id[8] < 0)
                exit(EXIT_FAILURE);

            if (child_id[8] == 0)
            {
                getDir(directoryList, dirPathEarth);
                if (removeBird(directoryList, dirPathEarth) == EXIT_FAILURE)
                    exit(EXIT_FAILURE);
            }
            else
            {
                while ((wait(&status[5]) > 0))
                    ;
                sleep(3);
                // printf("Nyampe sini kgk?\n");
                getDir(tempDirectoryList, dirPathWater);
                if (fileWrite(tempDirectoryList, dirPathWater) == EXIT_FAILURE)
                    exit(EXIT_FAILURE);
            }
        }
    }
    printf("Program selesai!\n");
    return 0;
}
