# Kelompok B11 Sistem Operasi 2022

## Anggota:
| Nama                          | NRP        |
|-------------------------------|------------|
| Haidar Fico Ramadhan Aryputra | 5025201185 |
| Nethaneel Patricio Linggar    | 5025201180 |

## Soal 1:
Dalam soal ini, diperlukan user untuk mendownload files dari link tertentu, lalu mengunzip file tersebut. Setelah file-file tersebut berhasil di-unzip menjadi dua folder
berupa `characters` dan `weapons`, zip file awal akan dihapus. Untuk melakukan hal-hal tersebut, menggunakan fungsi `downloadFiles`, `unzip`, dan `rmFile`, dengan kode sebagai
berikut:
```c
void downloadFiles(char downloadLink[], char outputName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"wget", downloadLink, "-O", outputName, NULL};
        execv("/bin/wget", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```
```c
void unzip(char zipName[])
{
    if (fork() == 0)
    {
        // Unzip Characters.zip
        char *arguments[] = {"unzip", zipName, NULL};
        execv("/usr/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```  
```c
void rmFile(char fileName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rm", "-r", fileName, NULL};
        execv("/usr/bin/rm", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```

Setelah bagian persiapan selesai, akan dibuat directory bernama `gacha_gacha` dengan function `makeDirectory` di dalam folder soal3.
```c
void makeDirectory(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mkdir", "-p", dirName, NULL};
        execv("/usr/bin/mkdir", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```

Bagian ini merupakan bagian terakhir yang dikerjakan di dalam terminal. Setelah ini, tugas-tugas selanjutnya akan diurusi oleh daemon yang dibuat dengan `createDaemon`. Kodenya
adalah sebagai berikut:
```c
pid_t createDaemon()
{
    pid_t pid, sid;

    pid = fork();
    if (pid > 0)
    {
        printf("Parent process akan diberhentikan\n");
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0)
        exit(EXIT_FAILURE);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    return sid;
}
```
Setelah daemon di-create, parent process akan diberhentikan dengan kode return 0. Daemon tidak akan memiliki akses terhadap STDIN, STDOUT, maupun STDERR sehingga filestream-filestream
tersebut perlu ditutup. Setelah daemon berhasil di-spawn, maka akan menggunakan sebuah array of FILE pointers untuk menyimpan pointer-pointer FILE ke setiap file di dalam folder character dan
weapons. Array of FILE pointers tersebut dinamakan weapons dan characters. Untuk mengisi array tersebut menggunakan function yang bernama `getDir` yang berisi sebagai berikut:
```c
void getDir(FILE *list[], char dirPath[])
{
    DIR *d;
    struct dirent *dir;
    int i = 0;
    char pathTemp[256];

    d = opendir(dirPath);

    while ((dir = readdir(d)) != NULL)
    {
        if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
        {
            sprintf(pathTemp, "%s%s", dirPath, dir->d_name);
            list[i] = fopen(pathTemp, "r+");
            i++;
        }
    }

    closedir(d);
    return;
}
```

Setelah ini, permainan gacha akan disapkan, pertama dengan mendeklarasi beberapa variabel yang dibutuhkan untuk permainan ini, yaitu `gachaCounter`, `primogems`, `tempFileString`, `tempFileStringArrCounter`,
`startTime`, `characterFileTotal`, dan `weaponFileTotal`. Deklarasi dari variabel-variabel tersebut adalah sebagai berikut:
```c
    unsigned int gachaCounter = 0;
    int primogems = 79000;
    char *tempFileString[10];
    short tempFileStringArrCounter = 0;
    time_t startTime = time(NULL);

    int weaponFileTotal = countFiles("./weapons/");
    int characterFileTotal = countFiles("./characters/");

    for (int i = 0; i < 10; i++)
    {
        tempFileString[i] = (char*)malloc(sizeof(char) * 100);
    }
``` 

Total primogems sesuai dengan ketentuan yang diberikan oleh soal. Untuk `weaponFileTotal` dan `characterFileTotal`, value mereka didapat dengan menggunakan function `countFiles`. Function tersebut
akan menerima input berupa filepath dan akan memberi output berupa total file di dalam directory tersebut. Function ini memiliki kode sebagai berikut:
```c
int countFiles(char path[])
{
    int count = 0;
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir)
        return -1;

    while ((dp = readdir(dir)) != NULL)
    { // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            count++;
        }
    }

    // Close directory stream
    closedir(dir);
    return count;
}
```

Mulainya game gacha dilakukan dengan memasuki while loop. While loop tersebut akan selesai ketika antara primogems sudah tidak mencukupi untuk melakukan sebuah gacha, atau tiga jam telah berlalu. Jika 
diantara kedua kondisi tersebut belum terjadi, maka setiap detik akan melakukan gacha. Logaritma gacha tersimpan di dalam function `gachaAlgorithm` yang akan memanggil function `__gachaAlgorithm` untuk
mengurusi bagian-bagian yang berurusan dengan pemilihan gachanya. Function `gachaAlgorithm` lebih mengurusi penulisan hasil dari gacha ke txt file dan pembuatan folder-folder. Kode `gachaAlgorithm` adalah
sebagai berikut:
```c
void gachaAlgorithm(FILE *characterList[], FILE *weaponsList[], unsigned int *gachaCounter, int *primogems, char *tempFileString[], time_t currTime, short *tempFileStringArrCounter, int weaponFileTotal, int characterFileTotal)
{
    (*primogems) -= gachaCost;
    (*gachaCounter)++;

    __gachaAlgorithm(characterList, weaponsList, tempFileString, tempFileStringArrCounter, weaponFileTotal, characterFileTotal, gachaCounter, primogems);

    if ((*gachaCounter) % 10 == 0)
    {

        char txtName[100];
        struct tm timeStruct = *localtime(&currTime);

        sprintf(txtName, "./gacha_gacha/%d_%d_%d_gacha_%d.txt", timeStruct.tm_hour, timeStruct.tm_min, timeStruct.tm_sec, *gachaCounter);

        FILE *writer = fopen(txtName, "w+");

        for (int i = 0; i < 10; i++)
        {
            fprintf(writer, "%s\n", tempFileString[i]);
            strcpy(tempFileString[i], "");
        }

        *tempFileStringArrCounter = 0;
        fclose(writer);
    }

    if ((*gachaCounter) % 90 == 0)
    {
        char dirName[100];
        char *dirList[50];
        FILE *txtList[100];
        char tempMoveDir[100];

        for (int i = 0; i < 50; i++)
        {
            dirList[i] = (char *)malloc(sizeof(char) * 50);
        }

        sprintf(dirName, "./gacha_gacha/total_gacha_%d", *gachaCounter);
        makeDirectory(dirName);

        getDirStringWithFilter("./gacha_gacha/", dirList, ".txt");

        for (int i = 0; strcmp("FINISHED", dirList[i]) != 0; i++)
        {
            sprintf(tempMoveDir, "./gacha_gacha/%s", dirList[i]);
            mvFile(tempMoveDir, dirName);
        }

        for (int i = 0; i < 50; i++)
        {
            free(dirList[i]);
        }
    }

    return;
}
```

Jika `gachaCounter` dapat dibagi 10, maka semua isi dari tempFileString akan ditulis ke dalam txt file sesuai format. Selain itu, ketika `gachaCounter` dapat dibagi 90, directory baru akan dibuat
sesuai ketentuan soal. Selain pembuatan directory baru, semua txt file juga akan dipindahkan ke dalam folder tersebut. Hal tersebut dilakukan dengan dua function `getDirStringWithFilter` dan
`mvFile`. Function kedua merupakan function wrapper untuk mv yang cukup sederhana. `getDirStringWithFilter` merupakan sebuah function yang mirip dengan `getDir`, tetapi function tersebut akan
memberi output berupa string dan akan memfilter hasil sesuai string filter yang dimasukkan ke dalam argumennya. Kodenya adalah sebagai berikut:
```c 
void mvFile(char filePath[], char destPath[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mv", filePath, destPath, NULL};
        execv("/usr/bin/mv", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```
```c
void getDirStringWithFilter(char dirPath[], char *dirList[], char filterPhrase[])
{
    DIR *d;
    struct dirent *dir;
    int i = 0;

    d = opendir(dirPath);

    while ((dir = readdir(d)) != NULL)
    {
        if (strstr(dir->d_name, filterPhrase))
        {
            strcpy(dirList[i], dir->d_name);
            i++;
        }
    }

    strcpy(dirList[i], "FINISHED");

    closedir(d);
    return;
}
```

String yang ditulis ke dalam txt file tersebut akan berasal dari array of string di dalam tempFileString. Array tersebut berisi dengan hasil-hasil gacha dengan kapasitas sepuluh. Pengisian dari array
tersebut menggunakan function `__gachaAlgorithm` yang akan menggunakan function `random` untuk memilih array index secara acak untuk memilih antara weapons atau characters. Karena data weapons
dan characters tersimpan di dalam bentuk JSON, maka diperlukan untuk mem-parse data JSON terlebih dahulu. Setelah dibaca, data tersebut akan ditulis ke dalam tempFileString sesuai dengan format.
Kode dari function ini adalah sebagai berikut:

```c
void __gachaAlgorithm(FILE *characterList[], FILE *weaponsList[], char *tempFileString[], short *tempFileStringArrCounter, int weaponFileTotal, int characterFileTotal, unsigned int *gachaCounter, int *primogems)
{
    if ((*tempFileStringArrCounter) % 2 == 0)
    {

        // weapons
        long RNG = random() % weaponFileTotal;

        char buffer[8192];
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        fread(buffer, 8192, 1, weaponsList[RNG]);
        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(tempFileString[*tempFileStringArrCounter], "%u_weapons_%d_%s_%d", *gachaCounter, json_object_get_int(rarity), json_object_get_string(name), *primogems);
    }
    else
    {
        // characters
        long RNG = random() % characterFileTotal;

        char buffer[8192];
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        fread(buffer, 8192, 1, characterList[RNG]);
        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(tempFileString[*tempFileStringArrCounter], "%u_characters_%d_%s_%d", *gachaCounter, json_object_get_int(rarity), json_object_get_string(name), *primogems);
    }

    (*tempFileStringArrCounter)++;
    return;
}
```

Ketika primogems sudah tidak mencukupi atau tiga jam telah berjalan, akan di-zip semua hasil dari gacha-gacha menjadi sebuah zip bernama `not_safe_for_wibu` dengan password `satuduatiga`. Hal ini
diurusi dengan function `gachaZip` yang berisi sebagai berikut:
```c 
void gachaZip()
{
    if (fork() == 0)
    {
        char *arguments[] = {"zip", "--password", "satuduatiga", "not_safe_for_wibu.zip", "-r", "./gacha_gacha/", "-m", NULL};
        execv("/usr/bin/zip", arguments);
        exit(EXIT_SUCCESS);
    }

    int status;
    while (wait(&status) >= 0)
        ;

    return;
}
```

Terakhir, akan dihapus semua data yang bukan zip tersebut, hal itu diurusi dengan `rmFile` dan `rmDir`.

Semua function-function ini akan dihubungkan dengan function `main` yang berisi sebagai berikut:
```c
int main()
{
    // Poin a
    downloadFiles("https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "Weapons.zip");
    downloadFiles("https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "Characters.zip");

    unzip("Characters.zip");
    unzip("Weapons.zip");

    rmFile("Weapons.zip");
    rmFile("Characters.zip");
    makeDirectory("gacha_gacha");

    // Membuat daemon, parent process akan diberhentikan
    pid_t processID = createDaemon();
    // Setelah ini, hanya daemon yang bekerja.

    FILE *weapons[150];
    FILE *characters[150];

    getDir(weapons, "./weapons/");
    getDir(characters, "./characters/");

    unsigned int gachaCounter = 0;
    int primogems = 79000;
    char *tempFileString[10];
    short tempFileStringArrCounter = 0;
    time_t startTime = time(NULL);

    int weaponFileTotal = countFiles("./weapons/");
    int characterFileTotal = countFiles("./characters/");

    for (int i = 0; i < 10; i++)
    {
        tempFileString[i] = (char *)malloc(sizeof(char) * 100);
    }

    while (1)
    {
        time_t currTime = time(NULL);

        if (primogems <= gachaCost || (startTime + 10800) < currTime)
        {
            // Akan berjalan ketika primogems habis OR waktu sudah mencapai 3 jam.
            gachaZip();
            break;
        }
        else
        {
            gachaAlgorithm(characters, weapons, &gachaCounter, &primogems, tempFileString, currTime, &tempFileStringArrCounter, weaponFileTotal, characterFileTotal);
        }

        sleep(1);
    }

    for (int i = 0; i < 10; i++)
    {
        free(tempFileString[i]);
    }

    rmFile("characters");
    rmFile("weapons");
    rmDir("characters");
    rmDir("weapons");

    return 0;
}
```

Setelah main return 0, maka daemon akan ditutup dan program akan selesai.

## Soal 2:
Dalam soal ini, diperlukan user untuk mengunzip isi dari `drakor.zip` yang penting, yaitu poster-poster, ke dalam `home/{user}/shift2/drakor/{kategori}` sesuai dengan penamaan dan kategori masing-masing. Lalu dalam tiap kategori terdapat `data.txt` yang akan menyimpan nama judul dan tahun rilis semua series pada kategori tersebut sesuai dengan format tertentu.
### Poin a
Pada poin ini, diminta untuk unzip `drakor.zip` dan menghilangkan isi-isinya yang tidak penting (folder-folder).
```c
void unzip(char directoryBase[], char directoryDest[]){
    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *args[] = {"unzip","-q",directoryBase,"*.png","-d",directoryDest,NULL};
        execv("/usr/bin/unzip",args);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}
```
Pemanggilan fungsi pada `int main()` ada sebagai berikut,
```c
unzip("./drakor.zip",dirPathTmp);
```
dimana `dirPathTmp` merupakan folder sementara untuk menyimpan foto-foto sebelum dimasukan ke dalam tiap kategori.

### Poin b
Pada poin ini, diminta untuk membuat folder untuk setiap kategori yang ada.
Caranya saya membuat fungsi untuk mendapatkan isi dari tiap file yang ada pada folder `home/{user}/tmp`.
```c
void runThruFiles(const char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir) 
        return; 

    while ((dp = readdir(dir)) != NULL){ // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            getGenre(dp->d_name);
        }
    }

    // Close directory stream
    closedir(dir);
}
```
Dalam function ini, dipanggil `getGenre()` pada tiap file, dimana `getGenre()` akan membuat folder untuk tiap kategori yang ada (dengan `createDir()`) dan membuat file `tmp.txt` yang akan menyimpan semua series berdasarkan tiap kategori mereka (dengan `createTxt()` untuk membuat tmp.txt dan `writeTxt()` untuk mengisinya). Isi dari `tmp.txt` dalam format `{tahun rilis}/{nama judul}` supaya diurutkan secara ascending terlebih dahulu sebelum masuk `data.txt`.
```c
void getGenre(char *name){
    char *userName = getlogin();
    char dirPathShift[100];
    char dirPath[100];
    strcpy(dirPathShift,"/home/");
    strcat(dirPathShift,userName);
    strcat(dirPathShift,"/shift2/drakor/");
    strcpy(dirPath,dirPathShift);

    int serialnumber = 0;
    char *contain;

    char *token = strtok(name, ";_");
    char judul[100];
    char tahun[100];
    while (token != NULL){
        if (serialnumber == 2){
            contain = strstr(token, ".png");
            if (contain){
                token[strlen(token)-4] = '\0';
            }
            strcat(dirPath,token);
            strcat(dirPath,"/");
            if (!isFileExists(dirPath)){
                createDir(dirPath);
                createTxt(dirPath,judul,tahun);
            } else {
                writeTxt(dirPath,judul,tahun);
            }
            strcpy(dirPath,dirPathShift);
            serialnumber = 0;
        }
        else if (serialnumber == 1){
            strcpy(tahun,token);
            serialnumber++;
        }
        else {
            strcpy(judul,token);
            serialnumber++;
        }
        token = strtok(NULL, ";_");
    }
}
```
`createDir()` menggunakan fork dan execv.
```c
void createDir(char directory[]){
    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", directory, NULL};
        execv("/usr/bin/mkdir", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```
### Poin c, d, & e
Pada poin ini
Dalam `int main()`, ada fungsi `runThruFolders()` yang akan menjalankan `compareDir()` dan `scanTmp()` pada tiap folder (atau kategori) di dalam `home/{user}/shift2/drakor/`.
```c
void runThruFolders(const char *path, const char *tempName){
    struct dirent *dp;
    DIR *dir = opendir(path);

    // char dirPathTmp[100]="/home/nethan/tmp/";
    char dirPathTmp[100]="/home/";
    strcat(dirPathTmp, tempName);
    strcat(dirPathTmp,"/tmp/");

    // Unable to open directory stream
    if (!dir) 
        return;

    while ((dp = readdir(dir)) != NULL){ // Iterate every folder
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            compareDir(dirPathTmp,dp->d_name);
            scanTmp(dp->d_name);
        }
    }

    // Close directory stream
    closedir(dir);
}
```
`compareDir()` akan membandingkan nama kategori dengan nama file-file yang ada pada `/home/{user}/tmp`. Jika nama file mengandung nama kategori, maka akan di-copy ke dalam kategori tersebut (dengan `copyPic()`) dengan penamaan judul yang sesuai.
```c
void compareDir(const char *path, char *folderName){
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir) 
        return;

    char *contain;

    while ((dp = readdir(dir)) != NULL){ // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            contain = strstr(dp->d_name,folderName);
            if (contain){
                copyPic(dp->d_name,folderName);
            }
        }
    }
    // Close directory stream
    closedir(dir);
}
```
Isi dari `copyPic()` adalah sebagai berikut.
```c
void copyPic(char *name, char *folder){
    char genre[BUFSIZ];

    char dirPathBase[100];
    char *tempName = (char*) malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathBase,"/home/");
    strcat(dirPathBase,tempName);

    char dirPathDest[100];
    strcpy(dirPathDest,dirPathBase);

    strcat(dirPathBase,"/tmp/");
    strcat(dirPathBase,name);
    strcat(dirPathDest,"/shift2/drakor/");
    strcat(dirPathDest,folder);
    
    int serialnumber = 0;
    char *contain;
    char judul[100];

    char *token = strtok(name, ";_");
    while (token != NULL){
        // contain = strstr(token, "_");
        if (serialnumber == 2){
            contain = strstr(token, ".png");
            if (contain){
                token[strlen(token)-4] = '\0';
            }
            if (!strcmp(token,folder)){
                strcat(dirPathDest,"/");
                strcat(dirPathDest,judul);
                strcat(dirPathDest,".png");
            }
            serialnumber = 0;
        }
        else if (serialnumber == 0){
            strcpy(judul,token);
            serialnumber++;
        }
        else{
            serialnumber++;
        }
        token = strtok(NULL, ";_");
    }

    int status;
	pid_t child_id = fork();
	if (child_id == 0) {
        char *argv[] = {"cp", dirPathBase, dirPathDest, NULL};
        execv("/usr/bin/cp",argv);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}
```
Setelah itu, dilakukan `scanTmp()` pada tiap folder, dimana akan menyortir secara ascending, meng-scan tiap `tmp.txt`, lalu di-print kembali sesuai dengan format yang sudah ditentukan. Setelah semua isi telah dipindahkan ke `data.txt`, file `tmp.txt` dihapus menggunakan `rm()`.
Berikut isi dari `scanTmp()`.
```c
void scanTmp(char *folder){
    char dirPathTmp[100];
    char dirPathData[100];
    char *tempName = (char*) malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathTmp,"/home/");
    strcat(dirPathTmp,tempName);
    strcat(dirPathTmp,"/shift2/drakor/");
    strcat(dirPathTmp,folder);
    strcpy(dirPathData,dirPathTmp);
    strcat(dirPathTmp,"/tmp.txt");
    strcat(dirPathData,"/data.txt");

    sort(dirPathTmp);

    char line[256];
    char nama[100];
    char rilis[100];

    FILE *ftmp = fopen(dirPathTmp, "r");
    FILE *fdatawrite = fopen(dirPathData, "w");
    FILE *fdataappend = fopen(dirPathData, "a");

	fprintf(fdatawrite, "kategori : %s\n\n", folder);
    while (fscanf(ftmp, "%s %s", rilis, nama) != EOF){
        fprintf(fdataappend, "nama : %s\nrilis: tahun %s\n\n", nama, rilis);
    }
    
	fclose(ftmp);
    fclose(fdatawrite);
    fclose(fdataappend);

    rm(dirPathTmp);
}
```
Folder `tmp` kemudian dihapus menggunakan `rm()` dengan kode sebagai berikut.
```c
void rm(char directory[]){
    int status;
	pid_t child_id = fork();
    if (child_id < 0) {
    	exit(EXIT_FAILURE);
  	}
	if (child_id == 0) {
        char *args[] = {"rm","-r",directory,NULL};
        execv("/usr/bin/rm",args);
        exit(EXIT_SUCCESS);
	} else {
		((wait(&status)) > 0);
	}
}
```

## Soal 3:

Pada soal ini, diberikan perintah untuk melakukan beberapa langkah. Langkah-
langkah tersebut merupakan:

- Membuat directory di `/home/[USER]/modul2/` dengan nama darat, lalu tiga detik kemudian dengan nama air.
- Unzip animal.zip ke dalam `/home/[USER]/modul2/`.
- Memisahkan hewan darat ke dalam folder darat dan hewan air ke folder air dengan jeda waktu 3 detik. Hewan yang tidak memiliki keterangan dihapus.
- Menghapus file-file burung di dalam directory darat.
- Membuat file list.txt di dalam folder air. File tersebut berisi list nama semua hewan dalam directory tersebut dengan format UID_[UID file permission]_Nama File.[jpg/png]

Untuk langkah pertama, dibuat sebuah function untuk membuar directory dengan nama darat lalu air. Function tersebut dinamakan `createDir` dengan kode sebagai berikut:
```c
int createDir(char directory[])
{
    char *argv[] = {"mkdir", "-p", directory, NULL};
    execv("/usr/bin/mkdir", argv);
    return 0;
}
```
Function ini cukup sederhana, hanya mengambil argumen berupa filepath directory,
lalu akan menjalankan command mkdir yang tersedia dalam bash dengan function
`execv`. 

Selanjutnya, akan mengunzip file animal.zip ke dalam directory tersebut. Hal ini
diurusi oleh function `unzip` yang memiliki kode sebagai berikut:
```c
int unzip(char directory[])
{
    char *arguments[] = {"unzip", "./animal.zip", "-d", directory, NULL};
    execvp("/usr/bin/unzip", arguments);
    return 0;
}
```
Function ini mirip dengan `createDir` secara struktur. Perbedaan utamanya berada
di dalam isi arguments dan command yang dijalankannya. Hazil unzip tersebut akan
disimpan di dalam `/home/[USER]/modul2/animal`.

Untuk perihal sorting antara binatang darat dan air, function `sortAnimalEarth` dan `sortAnimalWater` dibuat. 
Kedua function tersebut hampir sama, dengan perbedaan utama antara mereka berupa
string filter yang mereka pakai. Sebelum itu, diperlukan sebuah cara untuk memberi
data yang berupa isi directory ke dalam kedua function tersebut. Oleh karena itu,
dipilih pendekatan yang berupa penyimpanan isi directory ke dalam array of string (karena dalam bahasa C tidak terdapat implementasi string, maka akan dipakai array of char pointers.)
Hal ini dilakukan dengan function `getDir`. Kode dari function adalah sebagai berikut:
```c
int getDir(char *dirList[], char directory[])
{
    int i = 0;
    DIR *dp;
    struct dirent *ep;
    dp = opendir(directory);
    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            dirList[i] = ep->d_name;
            i++;
        }
    }
    return 0;
}
```
Output dari function ini akan disimpan di dalam dirList. Output tersebut
akan dimasukkan ke dalam kedua function sort dan beberapa function yang akan
datang yang membutuhkan data mengenai isi directory tertentu. Setelah mendapatkan
dirList, akan dimasukkan ke dalam function-function sort tersebut.
```c
int sortAnimalEarth(char *dirList[], char directoryAnimal[], char directoryEarth[])
{
    int i;
    int counter = 1;
    char *arguments[120];
    for (int k = 0; k < 120; k++)
        arguments[k] = malloc(100);
    char tempString[40];
    arguments[0] = "mv";
    for (i = 1; i < 20; i++)
    {
        if ((strstr(dirList[i - 1], "_darat")))
        {
            strcpy(tempString, directoryAnimal);
            strcat(tempString, dirList[i - 1]);
            strcpy(arguments[counter], tempString);
            counter++;
        }
    }
    arguments[counter] = directoryEarth;
    arguments[counter + 1] = NULL;
    execv("/usr/bin/mv", arguments);
    return 0;
}
```
```c
int sortAnimalWater(char *dirList[], char directoryAnimal[], char directoryWater[])
{
    int i;
    int counter = 1;
    char *arguments[120];
    for (int k = 0; k < 120; k++)
        arguments[k] = malloc(100);
    char tempString[40];
    arguments[0] = "mv";
    for (i = 1; i < 12; i++)
    {
        if ((strstr(dirList[i - 1], "_air")))
        {
            strcpy(tempString, directoryAnimal);
            strcat(tempString, dirList[i - 1]);
            strcpy(arguments[counter], tempString);
            counter++;
        }
    }

    arguments[counter] = directoryWater;
    arguments[counter + 1] = NULL;
    execv("/usr/bin/mv", arguments);
    return 0;
}
```
Setelah semua file yang memasuki kriteria telah dipindahkan ke dalam folder air
dan darat. Akan dipanggil function `removeDir` untuk men-delete folder animal
yang juga berisi file-file yang tidak dapat difilter.
```c
int removeDir(char directory[])
{
    char *arguments[4];
    arguments[0] = "rm";
    arguments[1] = "-r";
    arguments[2] = directory;
    arguments[3] = NULL;
    execv("/usr/bin/rm", arguments);
}
```
Setelah itu, perlu didelete file-file burung di dalam folder darat.
Untuk mencapai itu, memakai metode filtering yang mirip dengan kedua
function filtering sebelumnya, dengan perbedaan utama yang berupa command rm,
bukan mv. Kode dari function `removeBird` adalah sebagai berikut:
```c
int removeBird(char *dirList[], char directoryEarth[])
{
    int i;
    int counter = 2;
    char *arguments[120];
    for (int k = 0; k < 120; k++)
        arguments[k] = malloc(100);
    char tempString[40];
    arguments[0] = "rm";
    arguments[1] = "-r";
    for (i = 2; i < 14; i++)
    {
        if ((strstr(dirList[i - 2], "_bird")))
        {
            strcpy(tempString, directoryEarth);
            strcat(tempString, dirList[i - 2]);
            strcpy(arguments[counter], tempString);
            counter++;
        }
    }
    arguments[counter] = NULL;
    return 0;
}
```

Bagian terakhir, diperlukan untuk membuat sebuah file `list.txt`. Hal tersebut
terdapat dalam function `fileWrite` yaitu:
```c
int fileWrite(char *dirList[], char directoryWater[])
{
    int i = 0;
    char listFilePath[50];
    strcpy(listFilePath, directoryWater);
    strcat(listFilePath, "list.txt");
    FILE *fp = fopen(listFilePath, "w+");
    struct stat info;
    char filePath[100];
    char *username = getlogin();
    char userPermissions[4] = {'t', 'm', 'p', '\0'};
    while (i < 9)
    {
        if (strstr(dirList[i], "..") || (strcmp(".", dirList[i]) == 0))
        {
            i++;
            continue;
        }
        strcpy(filePath, directoryWater);
        strcat(filePath, dirList[i]);
        stat(filePath, &info);
        checkPermissions(userPermissions, &info);
        fprintf(fp, "%s_%s_%s\n", username, userPermissions, dirList[i]);
        i++;
    }
    return 0;
}
```

Data permissions didapatkan di dalam function `checkPermissions`. Output dari
function tersebut akan disimpan di dalam char array yang berukuran 4.

```c
void checkPermissions(char userPermissions[], struct stat *info)
{
    int counter = 0;
    if (info->st_mode & S_IRUSR)
    {
        userPermissions[counter] = 'r';
        counter++;
    }
    if (info->st_mode & S_IWUSR)
    {
        userPermissions[counter] = 'w';
        counter++;
    }
    if (info->st_mode & S_IXUSR)
    {
        userPermissions[counter] = 'x';
        counter++;
    }
    userPermissions[counter] = '\0';
    return;
}
```

Akan selalu ada character `/0` untuk menandakan akhir dari string tersebut. Hasil
dari semua data tersebut akan dimasukkan ke dalam `list.txt` untuk setiap file dalam directory.

Untuk menghubungkan semua function yang tertera, dibutuhkan sebuah main function.
Isi dari main function ini cukup sederhana:
- Melakukan `fork` untuk setiap proses. Hal ini dikarenakan function execv akan menyelesaikan proses yang menjalankan function tersebut.
- Melakukan function `sleep` untuk memberi jeda waktu sesuai dengan soal.

Kode dari main function adalah sebagai berikut:
```c
int main(int argc, char const *argv[])
{
    pid_t child_id[10];
    int status[10];
    char dirPathBase[100];
    char dirPathWater[100];
    char dirPathEarth[100];
    char dirPathAnimal[100];
    char *tempName = (char *)malloc(100 * sizeof(char));
    tempName = getlogin();
    strcpy(dirPathBase, "/home/");
    strcat(dirPathBase, tempName);
    // free(tempName);
    strcat(dirPathBase, "/modul2");
    strcpy(dirPathEarth, dirPathBase);
    strcpy(dirPathWater, dirPathBase);
    strcpy(dirPathAnimal, dirPathBase);
    strcat(dirPathWater, "/air/");
    strcat(dirPathEarth, "/darat/");
    strcat(dirPathAnimal, "/animal/");
    // printf("Does this work lol: %s\n",dirPathWater);
    child_id[0] = fork();
    // printf("This is child id %d\n",child_id[0]);
    // Process membuat directory
    if (child_id[0] < 0)
        exit(EXIT_FAILURE);

    if (child_id[0] == 0)
    {
        child_id[1] = fork();
        // printf("This is the fork's child id %d\n",child_id[1]);
        if (child_id[1] < 0)
            exit(EXIT_FAILURE);

        if (child_id[1] == 0)
            if (createDir(dirPathEarth) == EXIT_FAILURE)
                exit(EXIT_FAILURE);
            else
            {
                // printf("Nyampe sini\n");
                exit(EXIT_SUCCESS);
            }
        else
        {
            while (wait(&status[1]) > 0)
                ;
            // printf("This is before sleep\n");
            sleep(3);
            // printf("This is after sleep\n");
            if (createDir(dirPathWater) == EXIT_FAILURE)
            {
                // printf("Nyampe sini gasi?");
                exit(EXIT_FAILURE);
            }
            else
            {
                exit(EXIT_SUCCESS);
            }
        }
    }

    while ((wait(&status[0])) > 0)
        ;

    // printf("aPA INI JALAN DULUAN\n");
    child_id[3] = fork();
    char *directoryList[256];
    char *tempDirectoryList[256];
    // printf("Testing123\n");

    if (child_id[3] < 0)
        exit(EXIT_FAILURE);

    if (child_id[3] == 0)
    {
        child_id[4] = fork();

        if (child_id[4] < 0)
            exit(EXIT_FAILURE);

        if (child_id[4] == 0)
        {

            if (unzip(dirPathBase) == EXIT_FAILURE)
            {
                exit(EXIT_FAILURE);
            }
            exit(EXIT_SUCCESS);
        }
        // printf("Nyampe sini wey\n");
        if (child_id[4] > 0)
        {
            while (wait(&status[3]) > 0)
                ;
            // sleep(3);
            // printf("This is dirpathAnimal %s\n",dirPathAnimal);
            getDir(directoryList, dirPathAnimal);
            if (sortAnimalEarth(directoryList, dirPathAnimal, dirPathEarth) == EXIT_FAILURE)
            {
                exit(EXIT_FAILURE);
            }
        }

        // printf("Child process selesai\n");
        exit(EXIT_SUCCESS);
    }
    else
    {
        while (wait(&status[2]) > 0)
            ;
        sleep(3);

        child_id[5] = fork();

        if (child_id[5] < 0)
            exit(EXIT_FAILURE);

        if (child_id[5] == 0)
        {
            child_id[6] = fork();

            if (child_id[6] < 0)
                exit(EXIT_FAILURE);

            if (child_id[6] == 0)
            {

                getDir(directoryList, dirPathAnimal);

                // printf("Nyampe sini juga kok\n");
                if (sortAnimalWater(directoryList, dirPathAnimal, dirPathWater) == EXIT_FAILURE)
                {
                    exit(EXIT_FAILURE);
                }
            }

            else
            {
                while (wait(&status[5]) > 0)
                    ;
                // printf("Nyampe sini kok rek\n");
                if (removeDir(dirPathAnimal) == EXIT_FAILURE)
                {
                    exit(EXIT_FAILURE);
                }
            }
        }

        else
        {
            while (wait(&status[4]) > 0)
                ;
            child_id[8] = fork();
            if (child_id[8] < 0)
                exit(EXIT_FAILURE);

            if (child_id[8] == 0)
            {
                getDir(directoryList, dirPathEarth);
                if (removeBird(directoryList, dirPathEarth) == EXIT_FAILURE)
                    exit(EXIT_FAILURE);
            }
            else
            {
                while ((wait(&status[5]) > 0))
                    ;
                sleep(3);
                // printf("Nyampe sini kgk?\n");
                getDir(tempDirectoryList, dirPathWater);
                if (fileWrite(tempDirectoryList, dirPathWater) == EXIT_FAILURE)
                    exit(EXIT_FAILURE);
            }
        }
    }
    printf("Program selesai!\n");
    return 0;
}
```